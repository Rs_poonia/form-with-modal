import React, { useState } from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

// for style
import './style.css'

function FormModal({ setShow, show, submitHandler }) {
  const handleClose = () => setShow(false)

  const [name, setUserName] = useState('')
  const [userEmail, setUserEmail] = useState('')
  const [userPassword, setUserPassword] = useState('')

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        className="modal-flex"
      >
        <div className="modal-image" />
        <div className="form-wrapper">
          <h2>Sign up now</h2>
          <hr className="signup-bottom-border" />
          <form className="sign-up-form">
            <input
              placeholder="Full name"
              required
              onChange={(e) => setUserName(e.target.value)}
            />
            <input
              type="email"
              placeholder="Email"
              required
              onChange={(e) => setUserEmail(e.target.value)}
            />
            <input
              type="email"
              placeholder="password"
              required
              onChange={(e) => setUserPassword(e.target.value)}
            />
            <Button
              onClick={() => {
                setUserName('')
                setUserEmail('')
                setUserPassword('')
                submitHandler(name, userEmail, userPassword)
              }}
              disabled={!name || !userEmail || !userPassword}
            >
              Sign up
            </Button>
          </form>
          <p className="terms-text">
            By signing up you agree to the <span>Terms and condition</span>
          </p>
        </div>
      </Modal>
    </>
  )
}
export default FormModal
