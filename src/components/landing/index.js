import React, { useState } from 'react'
import MenuIcon from '../images/menuIcon'
import FormModal from '../modal'
import Button from 'react-bootstrap/Button'
// for use css
import './style.css'
const LIGHT_CLASS = 'light-bg'
const DARK_CLASS = 'dark-bg'

export default function HomePage() {
  /**
   * for maintaining bg color
   */
  const [isLightTheme, toggleTheme] = useState(true)
  /**
   * for maintaining modal for sign in
   */
  const [showModal, setShowModal] = useState(false)
  const [isSubmitted, setIsSubmitted] = useState(false)
  const toggleThemeHandler = () => {
    toggleTheme(!isLightTheme)
  }
  /**
   * for handle open form for signin
   */
  const signInHandler = () => {
    setShowModal(true)
  }
  const submitHandler = (name, email, password) => {
    setShowModal(false)
    console.log('user has successfully signup', name, email, password)
    setIsSubmitted(true)
  }
  return (
    <section className={isLightTheme ? LIGHT_CLASS : DARK_CLASS}>
      <FormModal
        show={showModal}
        setShow={setShowModal}
        submitHandler={submitHandler}
      />
      <div className="heading-text">
        <h1>Lucky</h1>
        <span className="menu-icon" onClick={() => toggleThemeHandler()}>
          <MenuIcon />
        </span>
      </div>
      <div className="main-text">
        <div className="mobile-image" />
        <div>
          <h2>One Touch for</h2>
          <h2>All needs</h2>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries,
          </p>
          {isSubmitted ? (
            <Button onClick={() => setIsSubmitted(false)}>Sign out</Button>
          ) : (
            <Button onClick={() => signInHandler()}>Sign up</Button>
          )}
        </div>
      </div>
    </section>
  )
}
