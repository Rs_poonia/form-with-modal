import React from 'react'
import HomePage from './components/landing'
function App() {
  return (
    <div className="App">
      <HomePage />
    </div>
  )
}

export default App
